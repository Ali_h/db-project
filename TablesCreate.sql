CREATE TYPE personnal_type AS ENUM ('poshtiban', 'nassab', 'tamirkar' , 'foroosh', 'anbar', 'hamahangi');
CREATE TABLE tbl_personnels (
	
  	personnel_id        		serial NOT NULL,
	fullname            		varchar(50) NOT NULL,
	mobile_number       		varchar(20) NOT NULL UNIQUE,
	personnel_address   		varchar(150) NOT NULL,
	active_condition 			BOOLEAN NOT NULL DEFAULT 'true',
	personnel_type VARCHAR(50) NOT NULL,
	PRIMARY KEY (personnel_id)

);


CREATE TABLE tbl_custumers (

	custumer_id								serial NOT NULL,
	fullname 								varchar(50) NOT NULL,
	nat_code 								varchar(15) NOT NULL UNIQUE,
	address 								varchar(150) NOT NULL,
	phone_number 							varchar(30) NOT NULL UNIQUE,
  PRIMARY KEY (custumer_id)

);


CREATE TABLE tbl_services (

	service_id 								serial NOT NULL,
	name 									varchar(50) NOT NULL,
	traffic 								varchar(10) NOT NULL,
	price 									numeric NOT NULL,
	active_condition 						BOOLEAN NOT NULL,
	expire_day_count 						int NOT NULL,
	PRIMARY KEY (service_id)

);


CREATE TABLE tbl_physical_commodity (

	commodity_id				 		serial NOT NULL,
	name 								varchar(50) NOT NULL,
	purchase_price 						numeric NOT NULL,
	sell_price 							numeric NOT NULL,
	enter_date 							timestamp  NOT NULL,
	count 								int NOT NULL,
	PRIMARY KEY (commodity_id)

);


CREATE TABLE tbl_sell_service_custumer (

	service_id							serial NOT NULL,
	custumer_id 						serial NOT NULL,
	personnel_id 						serial NOT NULL,
	sell_date 							date NOT NULL,
	sell_type 							BOOLEAN NOT NULL,
	sell_price 							bigint NOT NULL,
	service_condition 					BOOLEAN NOT NULL,
	PRIMARY KEY (service_id , custumer_id , sell_date),
	FOREIGN KEY (service_id) REFERENCES tbl_services(service_id),
	FOREIGN KEY (custumer_id) REFERENCES tbl_custumers(custumer_id),
	FOREIGN KEY (personnel_id) REFERENCES tbl_personnels(personnel_id)

);


CREATE TABLE tbl_extended_service_custumer (

	service_id 							serial NOT NULL,
	custumer_id 						serial NOT NULL,
	personnel_id 						serial NOT NULL,
	extended_date 						date NOT NULL,
	extended_price 						bigint NOT NULL,
	extended_condition 					BOOLEAN NOT NULL,
	PRIMARY KEY (service_id , custumer_id , extended_date),
	FOREIGN KEY (service_id) REFERENCES tbl_services(service_id),
	FOREIGN KEY (custumer_id) REFERENCES tbl_custumers(custumer_id),
	FOREIGN KEY (personnel_id) REFERENCES tbl_personnels(personnel_id)

);


CREATE TABLE tbl_sell_physicalCommodity_custumer (

	physical_commodity_id	 			serial NOT NULL,
	custumer_id 						serial NOT NULL,
	sell_date 							timestamp  NOT NULL,
	count_sell 							int NOT NULL,
	sell_price 							numeric NOT NULL,
	personnel_id 						serial NOT NULL,
	PRIMARY KEY (physical_commodity_id , custumer_id , sell_date),
	FOREIGN KEY (physical_commodity_id) REFERENCES tbl_physical_commodity(commodity_id),
	FOREIGN KEY (custumer_id) REFERENCES tbl_custumers(custumer_id),
	FOREIGN KEY (personnel_id) REFERENCES tbl_personnels(personnel_id)
	
);



CREATE TABLE tbl_coordination_expert_custumer (

	personnel_id					 	serial NOT NULL,
	custumer_id 						serial NOT NULL,
	coordination_date 					timestamp NOT NULL,
	cost_condition 						BOOLEAN NOT NULL,
	cost 								bigint,
	serve_type 							BOOLEAN NOT NULL,
	details 							varchar(250),
	PRIMARY KEY (personnel_id , custumer_id , coordination_date),
	FOREIGN KEY (custumer_id) REFERENCES tbl_custumers(custumer_id),
	FOREIGN KEY (personnel_id) REFERENCES tbl_personnels(personnel_id)
	
);


CREATE TABLE tbl_permissions (

	permission_name 				varchar(30) NOT NULL,
	PRIMARY KEY (permission_name)

);



CREATE TABLE tbl_personnal_permissions (

	personnel_id 						bigint NOT NULL,
	permission_name 					varchar NOT NULL,
	PRIMARY KEY (personnel_id, permission_name),
	FOREIGN KEY (permission_name) REFERENCES tbl_permissions(permission_name),
	FOREIGN KEY (personnel_id) REFERENCES tbl_personnels(personnel_id)
	

);