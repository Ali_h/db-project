import psycopg2
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT



create_database_query = """  CREATE DATABASE mokhaberat
                             WITH OWNER = postgres
                             ENCODING = 'UTF8'; """
db_configs = {}
db_configs['host']='127.0.0.1'
db_configs['dbname']='mokhaberat'
db_configs['username']='postgres'
db_configs['password']='faridkh.007'
db_configs['port']='5432'
db_configs['dbname']='mokhaberat'                                     
                    

class Database:
    def __init__(self,_db_name=None):
        if _db_name:
            self.connect(mode=None,db_name=_db_name)
        else:
            self.connect()

    def __exit__(self):
        self.close()

    def connect(self,mode=None,db_name=None):
        if mode is None:
            self.conn = psycopg2.connect(user=db_configs['username'],password=db_configs['password'],host=db_configs['host'],dbname=db_name)
            self.cur = self.conn.cursor()
        elif mode=="ISOLATION_LEVEL_AUTOCOMMIT":
            self.conn = psycopg2.connect(user=db_configs['username'],password=db_configs['password'],host=db_configs['host'],dbname=db_name)
            self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            self.cur = self.conn.cursor()
        else:
            raise ValueError("mode is invalid")
            

    def commit(self):
        self.conn.commit()

    def close(self):
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def init_database(self):
        try:
            self.close()
            self.connect(mode="ISOLATION_LEVEL_AUTOCOMMIT")
            self.cur.execute(create_database_query)
            self.conn.commit()
        except Exception as dbError:
            print("database Error:", dbError)
        self.close()
        

    def init_tables(self):
        try:
            self.close()
            self.connect(mode="ISOLATION_LEVEL_AUTOCOMMIT",db_name=db_configs['dbname'])
            self.exec_sql_commands_from_file("TablesCreate.sql")
        except Exception as dbError:
            print("database Error:", dbError)
        

    def exe_query(self, query):
        result = {}
        result['data'] = {}
        result['status'] = False
        try:
            self.cur.execute(query)
            result['data'] = self.cur.fetchall()
            result["status"] = True
            return result
        except Exception as dbError:
            print("database Error:", dbError)

    def exec_command(self,query):
        result = {}
        result['status'] = False
        try:
            self.cur.execute(query)
            self.commit()
            result["status"] = True
            return result
        except Exception as dbError:
            print("database Error:", dbError)    

    def exec_sql_commands_from_file (self,path):
        file = open(path)
        data = file.read()
        file.close()
        sql_commands = data.split(';')
        for command in sql_commands:
            if command != '\n' and command != '':
                try:
                    command = command.replace('\n',' ')
                    self.exec_command(command)
                except Exception as dbError:
                    print("database Error:", dbError)




