//tbl_personnels
INSERT INTO tbl_personnels VALUES (, 'علی حسین پور' , '09390475656' , 'شاهین شهر' , 'true' , 'poshtiban') ;
INSERT INTO tbl_personnels VALUES (, 'نوید میر مقتدایی' , '09390475000' , 'اصفهان' , 'true' , 'nassab') ;
INSERT INTO tbl_personnels VALUES (, 'علی مرتضوی منش' , '09390888656' , 'قم' , 'true' , 'tamirkar') ;
INSERT INTO tbl_personnels VALUES (, 'فرید خویی' , '09397855656' , 'قم' , 'true' , 'foroosh') ;


//tbl_custumers
INSERT INTO tbl_custumers VALUES (, 'نگین اشرفی' , '5100158265' , 'اصفهان' , '09397855656') ;
INSERT INTO tbl_custumers VALUES (, 'مهران رجبی' , '5100150005' , 'اصفهان' , '09390888656') ;
INSERT INTO tbl_custumers VALUES (, 'معین اسماعیلی' , '1270158265' , 'تهران' , '09390475000') ;
INSERT INTO tbl_custumers VALUES (, 'امید محمدی' , '1278158265' , 'یزد' , '09390475656') ;


//tbl_services
INSERT INTO tbl_custumers VALUES (, 'سرویس حرفه ای 5 گیگ' , '5' , 50000 , 'true' , 30) ;
INSERT INTO tbl_custumers VALUES (, 'سرویس حرفه ای 10 گیگ' , '10' , 65000 , 'true' , 30) ;
INSERT INTO tbl_custumers VALUES (, 'سرویس طلایی شبانه' , '25' , 120000 , 'true' , 30) ;
INSERT INTO tbl_custumers VALUES (, 'سرویس طلایی حرفه ای' , '50' , 150000 , 'true' , 30) ;


//tbl_physical_commodity
INSERT INTO tbl_physical_commodity VALUES (, 'کابل تلفن' , 2000 , 2500 , '2017-03-01:18:23:34' , 0) ;
INSERT INTO tbl_physical_commodity VALUES (, 'روتر ۳ انتنه' , 65000 , 70000 , '2017-05-01:18:29:34' , 5) ;
INSERT INTO tbl_physical_commodity VALUES (, 'روتر ۲ انتنه' , 40000 , 45000 , '2017-02-01:18:27:34' , 20) ;
INSERT INTO tbl_physical_commodity VALUES (, 'هاب' , 5000 , 6000 , '2017-03-02:18:23:34' , 10) ;


//tbl_sell_service_custumer
INSERT INTO tbl_physical_commodity VALUES (1,2,1, '2017-11-01:31:00:34' , 'true' , 65000 , 'true') ;
INSERT INTO tbl_physical_commodity VALUES (3,1,3, '2017-05-01:18:10:34' , 'true' , 72000 , 'true') ;
INSERT INTO tbl_physical_commodity VALUES (3,1,4, '2017-02-01:20:23:34' , 'true' , 53000 , 'true') ;
INSERT INTO tbl_physical_commodity VALUES (2,4,1, '2017-01-01:18:29:34' , 'true' , 63000 , 'true') ;



//tbl_extended_service_custumer
INSERT INTO tbl_physical_commodity VALUES (3,1,4, '2017-02-01:20:23:34' , 15000 , 'true') ;
INSERT INTO tbl_physical_commodity VALUES (2,3,1, '2017-01-01:18:29:34' , 20000 , 'true') ;
INSERT INTO tbl_physical_commodity VALUES (3,3,1, '2017-11-01:31:00:34' , 15000 , 'true') ;
INSERT INTO tbl_physical_commodity VALUES (3,1,3, '2017-05-01:18:10:34' , 30000 , 'true') ;



//tbl_sell_physicalCommodity_custumer
INSERT INTO tbl_physical_commodity VALUES (3,1, '2017-02-01:20:23:34' , 10 ,  32000 , 1) ;
INSERT INTO tbl_physical_commodity VALUES (2,3, '2017-01-01:18:29:34' , 09 ,  15000 , 4) ;
INSERT INTO tbl_physical_commodity VALUES (3,3, '2017-11-01:31:00:34' , 06 ,  30000 , 3) ;
INSERT INTO tbl_physical_commodity VALUES (3,1, '2017-05-01:18:10:34' , 11 ,  11000 , 1) ;


//tbl_coordination_expert_custumer
INSERT INTO tbl_physical_commodity VALUES (4,1, '2017-02-01:20:23:34' , 'true' ,  11000 , 'true' ,'رفع خرابی') ;
INSERT INTO tbl_physical_commodity VALUES (2,3, '2017-01-01:18:29:34' , 'true' ,  2000 , 'true' , 'عدم رفع خرابی') ;
INSERT INTO tbl_physical_commodity VALUES (1,4, '2017-11-01:31:00:34' , 'true' ,  11000 , 'true' ,'رفع خرابی') ;
INSERT INTO tbl_physical_commodity VALUES (1,1, '2017-05-01:18:10:34' , 'false' ,  0 , 'true' ,'رفع خرابی') ;


//tbl_permissions
INSERT INTO tbl_physical_commodity VALUES ('مدیر') ;
INSERT INTO tbl_physical_commodity VALUES ('کارمند') ;




//tbl_personnal_permissions
INSERT INTO tbl_physical_commodity VALUES (1,'مدیر') ;
INSERT INTO tbl_physical_commodity VALUES (2,'کارمند') ;
