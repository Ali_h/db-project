from db import Database
db_conn = Database(_db_name='mokhaberat')
class Personnel:
    def __init__(self):
        self.table_name = 'tbl_services'
    def add_Personnel(self,personell_obj):
        command = "insert into tbl_personnels(fullname,mobile_number,personnel_address,active_Condition,personnel_type) values ('{}','{}','{}','{}','{}') ".format(personell_obj['fullname'],personell_obj['mobile_number'],personell_obj['personnel_address'],personell_obj['active_Condition'],personell_obj['personnel_type'])
        db_conn.exec_command(command)
    def update_Personnel(self,personell_obj,personell_id):
        command = "update tbl_personnels set fullname='{}',mobile_number='{}',personnel_address='{}',active_Condition='{}',personnel_type='{}' where personell_id ={}".format(personell_obj['fullname'],personell_obj['mobile_number'],personell_obj['personnel_address'],personell_obj['active_Condition'],personell_obj['personnel_type'],personell_id)
        db_conn.exec_command()
    def delete_Personnel(self,personell_id):
        command = "delete from tbl_personnels where personnel_id = {}".format(personell_id)
        db_conn.exec_command(command)
    

class Customer:
    def __init__(self):
        self.table_name = 'tbl_custumers'

    def add_Customer(self,customer_obj):
        command = "insert into tbl_custumers(fullname,nat_code,address,phone_number) values ('{}','{}','{}','{}') ".format(customer_obj['fullname'],customer_obj['nat_code'],customer_obj['address'],customer_obj['phone_number'])
        db_conn.exec_command(command)

    def update_Customer(self,customer_obj,custumer_id):
        command = "update tbl_custumers set fullname='{}',nat_code='{}',address='{}',phone_number='{}' where custumer_id ={}".format(customer_obj['fullname'],customer_obj['nat_code'],customer_obj['address'],customer_obj['phone_number'],custumer_id)
        db_conn.exec_command()

    def delete_Customer(self,custumer_id):
        command = "delete from tbl_custumers where custumer_id = {}".format(custumer_id)
        db_conn.exec_command(command)


class Service:
    def __init__(self):
        self.table_name = 'tbl_services'

    def add_Service(self,service_obj):
        command = "insert into tbl_services(name,traffic,price,active_condition,expire_day_count) values ('{}','{}',{},'{}',{}) ".format(service_obj['name'],service_obj['traffic'],service_obj['price'],service_obj['active_condition'],service_obj['expire_day_count'])
        db_conn.exec_command(command)

    def update_Service(self,service_obj,service_id):
        command = "update tbl_services set name='{}',traffic='{}',price={},active_condition='{}',expire_day_count={} where service_id ={}".format(service_obj['name'],service_obj['traffic'],service_obj['price'],service_obj['active_condition'],service_obj['expire_day_count'],service_id)
        db_conn.exec_command()

    def delete_Service(self,service_id):
        command = "delete from tbl_services where service_id = {}".format(service_id)
        db_conn.exec_command(command)


class PhysicalCommodity:
    def __init__(self):
        self.table_name = 'tbl_physical_commodity'

    def add_PhysicalCommodity(self,physicalcommodity_obj):
        command = "insert into tbl_physical_commodity(name,purchase_price,sell_price,enter_date,count) values ('{}',{},{},'{}',{}) ".format(physicalcommodity_obj['name'],physicalcommodity_obj['purchase_price'],physicalcommodity_obj['sell_price'],physicalcommodity_obj['enter_date'],physicalcommodity_obj['count'])
        db_conn.exec_command(command)

    def update_PhysicalCommodity(self,physicalcommodity_obj,commodity_id):
        command = "update tbl_physical_commodity set name='{}',purchase_price={},sell_price={},enter_date='{}',count={} where commodity_id ={}".format(physicalcommodity_obj['name'],physicalcommodity_obj['purchase_price'],physicalcommodity_obj['sell_price'],physicalcommodity_obj['enter_date'],physicalcommodity_obj['count'],commodity_id)
        db_conn.exec_command()

    def delete_PhysicalCommodity(self,commodity_id):
        command = "delete from tbl_physical_commodity where commodity_id = {}".format(commodity_id)
        db_conn.exec_command(command)


class SellService:
    def __init__(self):
        self.table_name = 'tbl_sell_service_custumer'

    def add_SellService(self,sellservice_obj):
        command = "insert into tbl_sell_service_custumer values ({},{},{},'{}','{}',{},'{}') ".format(sellservice_obj['service_id'],sellservice_obj['custumer_id'],sellservice_obj['personnel_id'],sellservice_obj['sell_date'],sellservice_obj['sell_type'],sellservice_obj['sell_price'],sellservice_obj['service_condition'])
        db_conn.exec_command(command)

    def update_SellService(self,sellservice_obj,service_id,custumer_id,sell_date):
        command = "update tbl_sell_service_custumer set service_id={},custumer_id={},personnel_id={},sell_date='{}',sell_type='{}',sell_price={},service_condition='{}' where service_id = {} and custumer_id = {} and sell_date = '{}'".format(sellservice_obj['service_id'],sellservice_obj['custumer_id'],sellservice_obj['personnel_id'],sellservice_obj['sell_date'],sellservice_obj['sell_type'],sellservice_obj['sell_price'],sellservice_obj['service_condition'],service_id,custumer_id,sell_date)
        db_conn.exec_command()

    def delete_SellService(self,service_id,custumer_id,sell_date):
        command = "delete from tbl_sell_service_custumer where service_id = {} and custumer_id = {} and sell_date = '{}'".format(service_id,custumer_id,sell_date)
        db_conn.exec_command(command)




class ExtendedService:
    def __init__(self):
        self.table_name = 'tbl_extended_service_custumer'

    def add_ExtendedService(self,extendedservice_obj):
        command = "insert into tbl_extended_service_custumer values ({},{},{},'{}',{},'{}') ".format(extendedservice_obj['service_id'],extendedservice_obj['custumer_id'],extendedservice_obj['personnel_id'],extendedservice_obj['extended_date'],extendedservice_obj['extended_price'],extendedservice_obj['extended_condition'])
        db_conn.exec_command(command)

    def update_ExtendedService(self,extendedservice_obj,service_id,custumer_id,extended_date):
        command = "update tbl_extended_service_custumer set service_id={},custumer_id={},personnel_id={},extended_date='{}',extended_price={},extended_condition='{}' where service_id = {} and custumer_id = {} and extended_date = '{}'".format(extendedservice_obj['service_id'],extendedservice_obj['custumer_id'],extendedservice_obj['personnel_id'],extendedservice_obj['extended_date'],extendedservice_obj['extended_price'],extendedservice_obj['extended_condition'],service_id,custumer_id,extended_date)
        db_conn.exec_command()

    def delete_ExtendedService(self,service_id,custumer_id,extended_date):
        command = "delete from tbl_extended_service_custumer where service_id = {} and custumer_id = {} and extended_date = '{}'".format(service_id,custumer_id,extended_date)
        db_conn.exec_command(command)


class PhysicalCommodity:
    def __init__(self):
        self.table_name = 'tbl_sell_physicalCommodity_custumer'

    def add_PhysicalCommodity(self,physicalcommodity_obj):
        command = "insert into tbl_sell_physicalCommodity_custumer values ({},{},'{}',{},{},{}) ".format(physicalcommodity_obj['physical_commodity_id'],physicalcommodity_obj['custumer_id'],physicalcommodity_obj['sell_date'],physicalcommodity_obj['count_sell'],physicalcommodity_obj['sell_price'],physicalcommodity_obj['personnel_id'])
        db_conn.exec_command(command)

    def update_PhysicalCommodity(self,physicalcommodity_obj,physical_commodity_id,custumer_id,sell_date):
        command = "update tbl_sell_physicalCommodity_custumer set physical_commodity_id={},custumer_id={},sell_date='{}',count_sell={},sell_price={},personnel_id={} where physical_commodity_id = {} and custumer_id = {} and sell_date = '{}'".format(physicalcommodity_obj['physical_commodity_id'],physicalcommodity_obj['custumer_id'],physicalcommodity_obj['sell_date'],physicalcommodity_obj['count_sell'],extendedservice_obj['sell_price'],extendedservice_obj['personnel_id'],physical_commodity_id,custumer_id,sell_date)
        db_conn.exec_command()

    def delete_PhysicalCommodity(self,physical_commodity_id,custumer_id,sell_date):
        command = "delete from tbl_sell_physicalCommodity_custumer where physical_commodity_id = {} and custumer_id = {} and sell_date = '{}'".format(service_id,custumer_id,extended_date)
        db_conn.exec_command(command)


class CoordinationExpert:
    def __init__(self):
        self.table_name = 'tbl_coordination_expert_custumer'

    def add_CoordinationExpert(self,coordinationexpert_obj):
        command = "insert into tbl_coordination_expert_custumer values ({},{},'{}','{}',{},'{}','{}') ".format(coordinationexpert_obj['personnel_id'],coordinationexpert_obj['custumer_id'],coordinationexpert_obj['coordination_date'],coordinationexpert_obj['cost_condition'],coordinationexpert_obj['cost'],coordinationexpert_obj['serve_type'],coordinationexpert_obj['details'])
        db_conn.exec_command(command)

    def update_CoordinationExper(self,coordinationexpert_obj,personnel_id,custumer_id,coordination_date):
        command = "update tbl_coordination_expert_custumer set personnel_id={},custumer_id={},coordination_date='{}',cost_condition='{}',cost={},serve_type='{}',details='{}' where personnel_id = {} and custumer_id = {} and coordination_date = '{}'".format(coordinationexpert_obj['personnel_id'],coordinationexpert_obj['custumer_id'],coordinationexpert_obj['coordination_date'],coordinationexpert_obj['cost_condition'],extendedservice_obj['cost'],extendedservice_obj['serve_type'],coordinationexpert_obj['details'],personnel_id,coordination_date)
        db_conn.exec_command()

    def delete_CoordinationExper(self,personnel_id,custumer_id,coordination_date):
        command = "delete from tbl_coordination_expert_custumer where personnel_id = {} and custumer_id = {} and coordination_date = '{}'".format(service_id,custumer_id,extended_date)
        db_conn.exec_command(command)



class Permission:
    def __init__(self):
        self.table_name = 'tbl_permission'

    def add_Permissions(self,permissions_obj):
        command = "insert into tbl_permission values ('{}') ".format(permissions_obj['permission_name'])
        db_conn.exec_command(command)

    def update_Permissions(self,permissions_obj,permission_name):
        command = "update tbl_permission set permission_name='{}' where permission_name = '{}' ".format(permissions_obj['permission_name'])
        db_conn.exec_command()

    def delete_Permissions(self,permission_name):
        command = "delete from tbl_permission where permission_name = '{}' ".format(permission_name)
        db_conn.exec_command(command)


class PersonnalPermission:
    def init(self):
        self.table_name = 'tbl_personnal_permissions'

    def add_Permissions(self,PersonnalPermissions_obj):
        command = "insert into tbl_personnal_permissions values ({},'{}') ".format(personnalpermissions_obj['permission_id'],personnalpermissions_obj['permission_name'])
        db_conn.exec_command(command)

    def update_Permissions(self,PersonnalPermissions_obj,personnel_id,permission_name):
        command = "update tbl_personnal_permissions set personnel_id={}, permission_name='{}', where personnel_id={} and permission_name = '{}' ".format(personnalpermissions_obj['permission_id'],personnalpermissions_obj['permission_name'])
        db_conn.exec_command()

    def delete_Permissions(self,PersonnalPermissions_name):
        command = "delete from tbl_personnal_permissions where personnel_id={} and permission_name = '{}' ".format(permission_name)
        db_conn.exec_command(command)