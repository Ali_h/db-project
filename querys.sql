//علی حسین پور
1)
SELECT COUNT(service_id)
FROM tbl_services left outer join  tbl_sell_service_custumer on tbl_services.service_id = tbl_sell_service_custumer.service_id
WHERE custumer_id = null and personnel_id =  null ;


2)
WITH count_service (services_id , services_count) as
    (SELECT service_id , COUNT(service_id)
     FROM tbl_sell_service_custumer 
     WHERE sell_price > 50000
     GROUP BY service_id
     HAVING COUNT(service_id) > 10 )
SELECT COUNT(services_id)
FROM count_service ;


3)
WITH personnel_name (personnels_name , personnels_count) as
    (SELECT fullname , COUNT(personnel_id)
     FROM tbl_personnels natural join  tbl_sell_service_custumer 
     WHERE sell_date > '2014-03-01' and sell_date < '2017-07-01'
     GROUP BY fullname )
SELECT personnels_name , MAX(personnels_count)
FROM personnel_name
GROUP BY personnels_name ;


4)
WITH services (services_id , services_count) as
    (SELECT service_id , COUNT(service_id)
     FROM tbl_sell_service_custumer
     GROUP BY service_id )
SELECT services_id , MIN(services_count)
FROM services
GROUP BY services_id ;


5)
SELECT COUNT(physical_commodity_id)
FROM (SELECT physical_commodity_id , COUNT(physical_commodity_id)
      FROM tbl_sell_physicalCommodity_custumer
      WHERE sell_price > 40000
      GROUP BY physical_commodity_id
      HAVING COUNT(physical_commodity_id) > 5) Physical_Commodity;


6)
SELECT "name" 
FROM tbl_services natural join tbl_extended_service_custumer natural join tbl_personnels 
WHERE fullname = 'علی' and extended_date = '2017-07-01' and extended_condition = 0;


7)
SELECT fullname as custumer_name
FROM tbl_custumers inner join tbl_sell_service_custumer
WHERE sell_date = '2017-03-01' and service_condition = 0 ;


8)
SELECT tbl_services.name
FROM tbl_services left outer join  tbl_extended_service_custumer on tbl_services.service_id = tbl_extended_service_custumer.service_id
WHERE custumer_id = null and personnel_id =  null ;



//محمد خویی
1)
SELECT "name" as service_name,sum(sell_price) as total_sell_price 
from tbl_sell_service_custumer NATURAL JOIN tbl_services
WHERE sell_date >= '2017-02-01' AND sell_date <  '2017-03-01'
GROUP BY service_name ORDER BY sell_date 


2)
SELECT sum(sell_price) as total_sell_price 
FROM tbl_sell_service_custumer NATURAL JOIN tbl_services NATURAL JOIN tbl_custumers NATURAL JOIN tbl_personnels
WHERE tbl_personnels.fullname = 'mohammad khooie' AND tbl_custumers.fullname = 'ali hosseinpoor' AND 
tbl_services."name" = 'tarhe talayi' AND sell_date >= '2017-02-01' AND sell_date <  '2017-03-01'


3)
SELECT "name" as service_name , count(tbl_coordination_expert_custumer.service_id) as coordination_count 
from tbl_coordination_expert_custumer NATURAL JOIN
tbl_services WHERE service_id in (
    SELECT service_id 
    	FROM tbl_sell_service_custumer
        GROUP BY service_id ORDER BY sum(sell_price) DESC LIMIT 5)
GROUP BY service_name 
HAVING count(service_id)>3


4)
(SELECT "name" as service_name  from tbl_sell_service_custumer NATURAL JOIN
tbl_services GROUP BY "name" ORDER BY sum(sell_price) DESC)
UNION (SELECT "name" as service_name  from tbl_sell_service_custumer NATURAL JOIN
tbl_services GROUP BY "name" ORDER BY count(service_id) DESC)


5)
SELECT "name" as service_name ,sum(sell_price) FROM tbl_sell_service_custumer NATURAL JOIN tbl_services WHERE 
service_id in (
	SELECT service_id FROM tbl_coordination_expert_custumer GROUP BY service_id ORDER BY count(service_id) LIMIT 5
)


6)
SELECT "name" as physicalcommodity FROM tbl_sell_physicalcommodity_custumer NATURAL JOIN tbl_physical_commodity WHERE
tbl_physical_commodity.count >1 


7)
SELECT "name" as service_name,count(tbl_extended_service_custumer.service_id) as extended_count 
FROM tbl_extended_service_custumer NATURAL JOIN tbl_services 
GROUP BY service_name ORDER BY extended_count 



//علی مرتضوی منش
1)
SELECT SUM (sell_price) AS total_service_sell_price
FROM tbl_sell_service_custumer NATURAL JOIN tbl_personnels NATURAL JOIN tbl_custumers
WHERE tbl_personnels.fullname = x AND tbl_custumers.fullname = y;


2)
(SELECT fullname AS personnel_name,SUM (sell_price)
FROM tbl_personnels NATURAL JOIN tbl_sell_service_custumer
WHERE sell_date >= '2017-05-04' AND sell_date < '2017-08-01'
GROUP BY personnel_name
ORDER BY SUM (sell_price) ASC)
union
(SELECT fullname AS personnel_name,COUNT (personnel_id)
FROM tbl_personnels NATURAL JOIN tbl_sell_service_custumer
WHERE sell_date >= '2017-05-04' AND sell_date < '2017-08-01'
GROUP BY personnel_name
ORDER BY COUNT (personnel_id) ASC);

3)
SELECT fullname AS personnel_name,COUNT(personnel_id)
FROM tbl_personnels NATURAL JOIN tbl_coordination_expert_custumer
GROUP BY personnel_name
ORDER BY COUNT (personnel_id) DESC ;

4)
SELECT fullname AS custumer_name
FROM tbl_extend NATURAL JOIN tbl_custumer
GROUP BY custumer_name
HAVING COUNT (custumer_id) >=n ;


5)
SELECT fullname AS custumer_name, COUNT(custumer_id)
FROM tbl_custumer NATURAL JOIN tbl_coordination_expert_custumer
GROUP BY custumer_name
ORDER BY COUNT (custumer_id) DESC ;

6)
SELECT physical_commodity_id, COUNT(custumer_id)
FROM tbl_custumer NATURAL JOIN tbl_sell_physicalCommodity_custumer
GROUP BY physical_commodity_id
ORDER BY COUNT (custumer_id) DESC ;

7)
SELECT tbl_physical_commodity.name, COUNT(commodity_id)
FROM tbl_physical_commodity left outer join  tbl_sell_physicalCommodity_custumer on tbl_physical_commodity.physical_commodity_id = tbl_sell_physicalCommodity_custumer.physical_id
WHERE custumer_id = null and personnel_id =  null ;

8)
WITH S_sum (total_service_sell_price) AS
    (SELECT SUM (sell_price) AS total_service_sell_price
    FROM tbl_sell_service_custumer NATURAL JOIN tbl_personnels NATURAL JOIN tbl_custumers
    WHERE tbl_personnels.fullname = x AND tbl_custumers.fullname = y)

WITH P_sum (total_physicalCommodity_sell_price) AS
    (SELECT SUM (sell_price) AS total_physicalCommodity_sell_price
    FROM tbl_sell_physicalCommodity_custumer NATURAL JOIN tbl_personnels NATURAL JOIN tbl_custumers
    WHERE tbl_personnels.fullname = x AND tbl_custumers.fullname = y)

SELECT S_sum.total_service_sell_price + P_sum.total_physicalCommodity_sell_price AS final_price;

